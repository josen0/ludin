/*
	Ludin v1.0
	Editor de texto y binarios interactivo orientado a linea de texto.

	Copyright (C) 2010, 2023 J. Nieto, all rights reserved.
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
   	the Free Software Foundation; either version 3, or (at your option)
   	any later version.

   	This program is distributed in the hope that it will be useful,
   	but WITHOUT ANY WARRANTY; without even the implied warranty of
   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

 	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	contact: kkeezff@hotmail.com

*/

#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <stdlib.h>

#define SIZE 128
#define stdinclr() while(getchar()!='\n');

unsigned char buf[SIZE];

void displayh(int ,long int );
void displaya(int);
void displayo(void);
long int nbloque(FILE *);
void marchivo(FILE *);
void iarchivo(FILE *, char *);
void barchivo(FILE *);
void darchivo(FILE *);
FILE *oarchivo(char *);

int _tmain(int argc, _TCHAR* argv[]) 
{
	FILE *fp=NULL;
	long int numread;
	unsigned char c;
	char *str;

	if(argc==1){
	fp=NULL;
		displayo();
		exit(1);
	}else{
	/* LEER y ESCRIBIR*/
	fp=oarchivo(argv[1]);

	}

	printf("Ludin  Copyright (C) 2010, 2023  J. Nieto\n");
    printf("This program comes with ABSOLUTELY NO WARRANTY.\n");
    printf("This is free software, and you are welcome to redistribute it\n");
	printf("under certain conditions.\n");

	do{
		printf("Ludin>");
		scanf("%c",&c);

		switch(c){
			case 'a':
			//ASCII
				displaya(numread);
				break;
			case 'b':
			//BUSCA CADENA
				barchivo(fp);
				break;
			/*
			case 'c':
			//CIERRA ARCHIVO ACTUAL
				if(fp!=NULL){
				fclose(fp);
				printf("Archivo cerrado.\n");
				fp=NULL;
				}else
				printf("No existe archivo a cerrar\n");
				break;
			*/
			case 'd':
			//MUESTRA 16 BYTES A PARTIR DEL OFFSET DADO
				darchivo(fp);
				break;
			case 'e':
				if(system(NULL)){
				printf("Procesador de comandos disponible.\n");
				printf("Comando: ");
				stdinclr();
				//%*c%[^\n]
				scanf("%[^\n]",str);
				//printf("Comando: %s\n",str);
				system(str);
				}
				else
				printf("Procesador de comandos no disponible.\n");
				break;
			case 'h':
			//HEXADECIMAL
				displayh(numread,ftell(fp));
				break;
			case 'i':
			//INFORMACION DEL ARCHIVO
				iarchivo(fp,argv);
				break;
			case 'm':
			//MODIFICAR EL ARCHIVO
				marchivo(fp);
				break;
			case 'n':
			//SELECCION DE NUMERO DE BLOQUE
				numread=nbloque(fp);
				break;
			/*
			case 'o':
				if(fp!=NULL)
				printf("Existe otro archivo abierto, cierrelo primero.\n");
				else{
				printf("Nombre del archivo: ");
				stdinclr();
				scanf("%as",&str);
				fp=oarchivo(str);
				if(fp!=NULL){
				printf("Archivo Abierto!\n");
				}else
				printf("Error\n");
				}
				break;
			*/
			case 's':
			//SALIR
				printf("Salir!\n");
				if(fp!=NULL)
				fclose(fp);
				break;
			case '?':
			//AYUDA
				printf("Ayuda\n");
				displayo();
				break;
		}
		stdinclr();
	}while(c!='s');

	return 0;
}

void displaya(int numread){
	short int i;
	for(i=0;i<numread;i++){
		if(isprint(buf[i]))
			printf("%c",buf[i]);
		else
			printf(".");
	}
	printf("\n");
}

void displayh(int numread, long int offset){
	short int i,j;
	offset-=numread;
	for(i=0;i<=numread/16;i++){
		if(numread==SIZE && i==8)
			continue;
		printf("%.5X\t",offset);
  			for(j=0;j<16;j++){
				if((i*16+j)<numread)
					printf("%.2X ",buf[i*16+j]);
				else
					printf("-- ");
				offset++;
			}
 		for(j=0;j<16;j++){
        		if(isprint(buf[i*16+j])){
				if((i*16+j)<numread)
					printf("%c",buf[i*16+j]);
				}else{
					if((i*16+j)<numread)
						printf(".");
				}
 	       }
 		printf("\n");
	}
 }
void displayo(void){
    printf("LUDIN\n");
	printf("Editor de archivos para linea de comandos\n");
	printf("Comandos:\n");
	printf("a\tvisualiza el archivo en modo Ascii.\n");
	printf("b\tBusca una cadena de caracteres.\n");
	//printf("c\tCierra el archivo actual.\n");
	printf("e\tEjecuta algun comando.\n");
	printf("h\tvisualiza el archivo en modo Hexadecimal.\n");
	printf("i\tmuestra Informacion del archivo actual.\n");
	printf("m\tModifica el archivo.\n");
	printf("n\tespecifica el Numero de bloque a visualizar.\n");
	//printf("o\tabre un nuevO archivo.\n");
	printf("s\tSalir de LUDIN.\n");
	printf("?\tAyuda.\n");
	}
long int nbloque(FILE *fp){
	long int numread, bloque;
	printf("Numero de bloque:");
	scanf("%ld",&bloque);
		if(fseek(fp,bloque*SIZE,SEEK_SET))
			printf("Error de seek.\n");
		numread=fread(buf,1,SIZE,fp);
		if(numread!=SIZE)
			printf("Se ha alcanzado el EOF.\n");
	return numread;
	}
void marchivo(FILE *fp){
	long int offst;
	unsigned char byte,opc;
	printf("OFFSET:");
	stdinclr();
	scanf("%lx",&offst);
	printf("BYTE:");
	stdinclr();
	scanf("%hx",&byte);
	printf("Byte: %#X, Offset: %#X\n",byte,offst);
	printf("Continuar? s/n ");
	stdinclr();
	scanf("%c",&opc);
		if(opc=='s'){
			fseek(fp,offst,SEEK_SET);
			fprintf(fp,"%c",byte);
			printf("Byte escrito con exito.\n");
		}else
			printf("Cancelado.\n");
	}
void iarchivo(FILE *fp,char *argv[]){
	fseek(fp,0,SEEK_END);
	printf("Archivo: %s\n",argv[1]);
	printf("Tamao: d:%ld h:%#X bytes\n",ftell(fp),ftell(fp));
	printf("Bloques de 128 bytes: %ld bloques\n",ftell(fp)/128);
}
void barchivo(FILE *fp){
	short i,nm;
	unsigned char opc,byte,*bscr;
	printf("Numero de caracteres a buscar:");
	stdinclr();
	scanf("%hd",&nm);
	bscr=(unsigned char *)malloc(sizeof(char)*nm);
	printf("Ascii  Hexadecimal? a/h ");
	stdinclr();
	scanf("%c",&opc);
		if(opc=='a'){
			printf("Inserte los caracteres.\n");
			for(i=0;i<nm;i++){
				printf("caracter %hd:",i+1);
				stdinclr();
				scanf("%c",&byte);
				*(bscr+i)=byte;
			}
		}
		if(opc=='h'){
			printf("Inserte los valores hexadecimales.\n");
			for(i=0;i<nm;i++){
				printf("caracter %hd:",i+1);
				stdinclr();
				scanf("%hx",&byte);
				*(bscr+i)=byte;
			}
		}
		printf("Caracteres:\n");
		for(i=0;i<nm;i++)
			printf("%#hX ",*(bscr+i));
		printf("\n");
		for(i=0;i<nm;i++)
			if(isprint(*(bscr+i)))
				printf("%c",*(bscr+i));
			else
				printf(".");
		printf("\n");
		printf("Continuar? s/n ");
		stdinclr();
		scanf("%c",&opc);
		i=0;
		if(opc=='s'){
			fseek(fp,0,SEEK_SET);
			while(!feof(fp)){
				if(getc(fp)==*(bscr+i))
					i++;
				else
					i=0;
				if(i==nm-1){
					printf("Concidencia: %#lX\n",ftell(fp)+1-nm);
					i=0;
				}
			}
		}else
			printf("Cancelado.\n");
		free(bscr);
}
void darchivo(FILE *fp){
	long int offst,numread;
	short int i;
	printf("OFFSET:");
	stdinclr();
	scanf("%lx",&offst);
	if(fseek(fp,offst,SEEK_SET))
		printf("Error de seek.\n");
	numread=fread(buf,1,16,fp);
	if(numread!=16)
		printf("Se ha alcanzado el EOF");
	for(i=offst;i<offst+numread;i++){
		printf("%.2X ",buf[i-offst]);
	}
	printf("\n");
	for(i=offst;i<offst+numread;i++){
		if(isprint(buf[i-offst]))
			printf("%c",buf[i-offst]);
		else
		printf(".");
	}
		printf("\n");
}
FILE *oarchivo(char *nombre){
	FILE *fp;
	if((fp=fopen(nombre,"rb+"))==NULL){
        	printf("No puede abrirse el archivo.\n");
		fp=NULL;
	}else
	return fp;
}
